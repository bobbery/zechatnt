#ifndef CONFIGFORM_H
#define CONFIGFORM_H

#include <QCheckBox>
#include <QWidget>
#include <QtSql>

namespace Ui {
class ConfigForm;
}

class ConfigForm : public QWidget
{
    Q_OBJECT

public:
    explicit ConfigForm(QWidget *parent = 0);
    ~ConfigForm();

private slots:
    void addName();
    void delName();
    void readCustomers();
    void delTask();

    void searchChanged();

private:
    Ui::ConfigForm *ui;
    QSqlTableModel *m_nameModel;
    QSqlTableModel *m_customerModel;
    QSortFilterProxyModel *m_customerProxyModel;
};


class CheckBoxDelegate: public QItemDelegate
{
    Q_OBJECT
public:
    CheckBoxDelegate(QObject *parent = 0);

    void paint( QPainter *painter,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;


    QWidget *createEditor( QWidget *parent,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;

    void setEditorData( QWidget *editor,
                        const QModelIndex &index ) const;

    void setModelData( QWidget *editor,
                        QAbstractItemModel *model,
                        const QModelIndex &index ) const;

    void updateEditorGeometry( QWidget *editor,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index ) const;

    mutable QCheckBox * theCheckBox;

private slots:

    void setData(bool val);


};




#endif // CONFIGFORM_H
