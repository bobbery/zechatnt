#include "configform.h"
#include "mainwindow.h"
#include "mapform.h"
#include "timeform.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QVBoxLayout *layout1 = new QVBoxLayout();
    layout1->addWidget(new MapForm(this));

    ui->tabMap->setLayout(layout1);

    QVBoxLayout *layout2 = new QVBoxLayout();
    layout2->addWidget(new TimeForm(this));

    ui->tabTime->setLayout(layout2);

    QVBoxLayout *layout3 = new QVBoxLayout();
    layout3->addWidget(new ConfigForm(this));

    ui->tabConfig->setLayout(layout3);
}

MainWindow::~MainWindow()
{
    delete ui;
}
