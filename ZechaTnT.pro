#-------------------------------------------------
#
# Project created by QtCreator 2016-04-09T18:32:29
#
#-------------------------------------------------

QT       += core gui network sql webenginewidgets widgets charts
CONFIG += C++11

TARGET = ZechaTnT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mapform.cpp \
    configform.cpp \
    timeform.cpp

HEADERS  += mainwindow.h \
    mapform.h \
    configform.h \
    timeform.h

FORMS    += mainwindow.ui \
    mapform.ui \
    configform.ui \
    timeform.ui

OTHER_FILES += \
    google_maps.html

RESOURCES += \
    resource.qrc
