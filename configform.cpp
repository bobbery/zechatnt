#include "configform.h"
#include "ui_configform.h"
#include <QFileDialog>
#include <QItemDelegate>

#include <QMessageBox>
#include <sstream>

CheckBoxDelegate::CheckBoxDelegate(QObject *parent ):QItemDelegate(parent)
{
}

void CheckBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    drawDisplay(painter,option,option.rect,index.model()->data( index, Qt::DisplayRole ).toBool()?QString("      ").append(tr("Ja")):QString("      ").append(tr("Nein")));
    drawFocus(painter,option,option.rect);
}

QWidget *CheckBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/*option*/, const QModelIndex &/*index*/) const
{
    theCheckBox = new QCheckBox( parent );
    QObject::connect(theCheckBox,SIGNAL(toggled(bool)),this,SLOT(setData(bool)));
    return theCheckBox;
}

void CheckBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    int val = index.model()->data( index, Qt::DisplayRole ).toInt();

    (static_cast<QCheckBox*>( editor ))->setChecked(val);

}

void CheckBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData( index, (int)(static_cast<QCheckBox*>( editor )->isChecked() ) );
}


void CheckBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/*index*/) const
{
    editor->setGeometry( option.rect );
}

void CheckBoxDelegate::setData(bool /*val*/)
{
    emit commitData(theCheckBox);
}




ConfigForm::ConfigForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfigForm)
{
    ui->setupUi(this);

    m_nameModel = new QSqlTableModel(this);
    m_nameModel->setTable("name");
    m_nameModel->setEditStrategy(QSqlTableModel::OnRowChange);
    m_nameModel->select();
    m_nameModel->setHeaderData(1, Qt::Horizontal, tr("Name"));

    ui->tvName->setModel(m_nameModel);
    ui->tvName->hideColumn(0);

    m_customerModel = new QSqlTableModel();
    m_customerModel->setTable("customer");
    m_customerModel->setEditStrategy(QSqlTableModel::OnFieldChange);
    m_customerModel->select();

    m_customerModel->setHeaderData(0, Qt::Horizontal, tr("Aktiv"));
    m_customerModel->setHeaderData(2, Qt::Horizontal, tr("Name"));
    m_customerModel->setHeaderData(3, Qt::Horizontal, tr("Adresse"));
    m_customerModel->setHeaderData(4, Qt::Horizontal, tr("Telefon"));

    m_customerProxyModel = new QSortFilterProxyModel(this);
    m_customerProxyModel->setSourceModel(m_customerModel);

    ui->tvCustomer->setModel(m_customerProxyModel);
    ui->tvCustomer->hideColumn(1); // Don't show ID
    ui->tvCustomer->setItemDelegateForColumn(0, new CheckBoxDelegate(this));
    ui->tvCustomer->verticalHeader()->setVisible(false);
    ui->tvCustomer->setSortingEnabled(true);

    for (int i = 1; i < 5; i++)
    {
        ui->tvCustomer->resizeColumnToContents(i);
    }

    connect(ui->btnAddName, SIGNAL(clicked()), this, SLOT(addName()));
    connect(ui->btnDelName, SIGNAL(clicked()), this, SLOT(delName()));
    connect(ui->btnReadCustomers, SIGNAL(clicked()), this, SLOT(readCustomers()));
    connect(ui->leSearch, SIGNAL(editingFinished()), this, SLOT(searchChanged()));
}

ConfigForm::~ConfigForm()
{
    delete ui;
}

void ConfigForm::addName()
{
    QSqlRecord rec = m_nameModel->record();

    rec.setNull(0);
    rec.setValue(1, QVariant("Leer"));

    m_nameModel->insertRecord(-1, rec);
    m_nameModel->submit();
    m_nameModel->select();
    ui->tvName->resizeColumnsToContents();
}

void ConfigForm::delName()
{
    m_nameModel->removeRow( ui->tvName->currentIndex().row() );
    m_nameModel->submit();
    m_nameModel->select();
    ui->tvName->resizeColumnsToContents();
}

struct CustomerInfo
{
    int id;
    QString name;
    QString address;
    QString phone;
};

CustomerInfo DetermineCustomerInfo(QByteArray const & line)
{
    CustomerInfo info;
    QString lineStr = QString::fromLatin1(line);
    QStringList list = lineStr.split(";");

    for (QString &str : list)
    {
        str = str.mid(1, str.size() - 2);
    }

    info.id = list.at(0).toInt();
    info.name = list.at(2) +  " " + list.at(3) + " " + list.at(4);
    info.name = info.name.simplified();
    info.address = list.at(5) +  " " + list.at(7) + " " + list.at(8);
    info.address = info.address.simplified();
    info.phone = list.at(9);
    info.phone = info.phone.simplified();

    return info;
}



void generateValues(QString &queryStr, CustomerInfo const &info)
{
    queryStr += "(";
    queryStr += "false";
    queryStr +=  ',';
    queryStr += QString::number(info.id);
    queryStr += ",'";
    queryStr += info.name;
    queryStr += "','";
    queryStr += info.address;
    queryStr += "','";
    queryStr += info.phone;
    queryStr += "'),";
}

void ConfigForm::readCustomers()
{
    QString fName = QFileDialog::getOpenFileName(this, "Adressdatei auswählen", ".", "*.txt");
    QFile file(fName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return;
    }

    QString headerStr = file.readLine(); // Skip header;

    if (!headerStr.startsWith("KU_NR"))
    {
        QMessageBox::warning(this, "Fehler", "falsche Datei !!!");
        return;
    }

    QSqlQuery deleteQuery("DELETE FROM customer");
    deleteQuery.exec();

    int count = 0;

    QString queryStr;

    queryStr = "INSERT INTO customer (active,id,name,address,phone) VALUES ";

    while (!file.atEnd())
    {
        QByteArray line = file.readLine();
        CustomerInfo info = DetermineCustomerInfo(line);

        generateValues(queryStr, info);

        if (count++ % 100 == 0)
        {
            queryStr.chop(1);
            qDebug() << queryStr;
            QSqlQuery query(queryStr);
            query.exec();
            queryStr = "INSERT INTO customer (active,id,name,address,phone) VALUES ";
        }
    }

    if (!queryStr.isEmpty())
    {
        queryStr.chop(1);
        QSqlQuery query(queryStr);
        query.exec();
    }

    qDebug() << "Einlesen fertig !!!";

    m_customerModel->select();
    ui->tvCustomer->resizeColumnsToContents();
}

void ConfigForm::delTask()
{
    m_customerModel->removeRow( ui->tvCustomer->currentIndex().row() );
    m_customerModel->submit();
    m_customerModel->select();
    ui->tvCustomer->resizeColumnsToContents();
}

void ConfigForm::searchChanged()
{
    if (ui->leSearch->text() == "++")
    {
        m_customerProxyModel->setFilterFixedString("1");
        m_customerProxyModel->setFilterKeyColumn(0);
    }
    else
    {
        m_customerProxyModel->setFilterRegExp(QRegExp(ui->leSearch->text(), Qt::CaseInsensitive, QRegExp::FixedString));
        m_customerProxyModel->setFilterKeyColumn(2);
    }
}

