#include "mainwindow.h"
#include <QApplication>
#include <QtSql>
#include <QSettings>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSettings settings("config.ini", QSettings::IniFormat);

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName(settings.value("HostName").toString());
    db.setDatabaseName(settings.value("DatabaseName").toString());
    db.setUserName(settings.value("UserName").toString());
    db.setPassword(settings.value("Password").toString());
    db.setPort(settings.value("Port").toInt());

    if (!db.open())
    {
        QMessageBox::critical(NULL, "Datenbankfehler", "Fehler beim Aufbauen der Datenbankverbindung<br> config.ini überprüfen");
        return -1;
    }

    MainWindow w;
    w.show();

    return a.exec();
}
