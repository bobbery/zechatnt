#ifndef MAPFORM_H
#define MAPFORM_H

#include <QWidget>

namespace Ui {
class MapForm;
}


class MapForm : public QWidget
{
    Q_OBJECT

public:
    explicit MapForm(QWidget *parent = 0);
    ~MapForm();

private slots:
    void showCoordinates(double east, double north);
    void setMarker(double east, double north, int name);

    void showAll();
    void somethingChanged();
    void itemClicked();
private:
    Ui::MapForm *ui;

    void drawLine(const QVector<QPair<double, double> > &coords);
};

#endif // MAPFORM_H
