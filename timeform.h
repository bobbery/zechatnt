#ifndef TIMEFORM_H
#define TIMEFORM_H

#include <QWidget>

namespace Ui {
class TimeForm;
}

class TimeForm : public QWidget
{
    Q_OBJECT

public:
    explicit TimeForm(QWidget *parent = 0);
    ~TimeForm();



private slots:
    void somethingChanged();

private:

    Ui::TimeForm *ui;
};

#endif // TIMEFORM_H
