#include "timeform.h"
#include "ui_timeform.h"

#include <QSqlQuery>
#include <QDebug>

TimeForm::TimeForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimeForm)
{
    ui->setupUi(this);

    connect(ui->cbName, SIGNAL(currentIndexChanged(int)), this, SLOT(somethingChanged()));
    connect(ui->deMonth, SIGNAL(dateChanged(const QDate &)), this, SLOT(somethingChanged()));

    ui->deMonth->setDate(QDate::currentDate());

    QSqlQuery query;
    query.exec("SELECT id, name FROM name");

    while (query.next())
    {
        ui->cbName->addItem(query.value(1).toString(), query.value(0));
    }
}

QString durationAsString(int seconds)
{
    QTime timeDiff(0,0,0);
    timeDiff = timeDiff.addSecs(seconds);
    return QString::number(seconds / 3600) + "h " + QString::number(timeDiff.minute()) + "m " + QString::number(timeDiff.second()) + "s";
}


TimeForm::~TimeForm()
{
    delete ui;
}


QString buildHtml(QSqlQuery &query)
{
    QString html;
    int workTime = 0;
    int taskTime = 0;
    int breakTime = 0;

    html += "<table id='fancytable1' style='width:100%'>";

    html += "<tr>";
    html += "<th>Kunde</th>";
    html += "<th>Start</th>";
    html += "<th>Ende</th>";
    html += "<th>Dauer</th>";
    html += "</tr>";

    while (query.next())
    {
        QDateTime fromTime = QDateTime::fromString(query.value(2).toString(), "yyyyMMddhhmmss");
        QDateTime toTime = QDateTime::fromString(query.value(3).toString(), "yyyyMMddhhmmss");

        int diff = fromTime.secsTo(toTime);

        QString desc = query.value(1).toString();

        if (query.value(0).toInt() == 2)
        {
            breakTime += diff;
        }
        else
        {
            if (desc.isEmpty())
            {
                desc = "Arbeitszeit";
            }
            else
            {
                taskTime += diff;
            }

            workTime += diff;
        }

        if (query.value(0).toInt() == 2)
        {
            desc = QString("PAUSE");
        }

        QTime timeDiff(0,0,0);
        timeDiff = timeDiff.addSecs(diff);

        html += "<tr>";
        html += "<td>" + desc + "</td>";
        html += "<td>" + fromTime.toString("dd MMM - hh:mm:ss") + "</td>";
        html += "<td>" + toTime.toString("dd MMM - hh:mm:ss") + "</td>";
        html += "<td>" + durationAsString(diff) + "</td>";
        html += "</tr>";
    }

    html += "<tr class='alt2'><td colspan=4>ZechaTnT</td></tr>";
    html += "</table>";

    html += "<h3>Arbeitszeit: " + durationAsString(workTime) + "</h3>";
    html += "<h3>Pausen: " + durationAsString(breakTime) + "</h3>";
    html += "<h3>Auftrag: " + durationAsString(taskTime) + "</h3>";
    html += "<h3>Unproduktive Zeit: " + durationAsString(workTime - taskTime) + "</h3>";

    html += "<div id='piechart_3d'></div>";

    return html;
}

QString htmlStyle();

void TimeForm::somethingChanged()
{

    QString html = htmlStyle();


    QDate date = ui->deMonth->date();
    int nameId = ui->cbName->currentData().toInt();

    QString timeStart = date.toString("yyyyMM") + "01000000";
    QString timeEnd = date.toString("yyyyMM") + "31235959";

    QString queryString = QString("SELECT type, customer.name, fromTime, toTime FROM time LEFT JOIN customer ON(time.customer = customer.id) WHERE time.name = %1 AND fromTime BETWEEN %2 AND %3").arg(nameId).arg(timeStart).arg(timeEnd);

    QSqlQuery query;
    query.exec(queryString);
    qDebug() << queryString;

    html += buildHtml(query);

    ui->textBrowser->setHtml(html);
}


QString htmlStyle()
{
    return "<style> \
    a:link {color:#000000;} \
    a:visited {color:#000000;} \
    a:hover {color:#81d50d;} \
    a:active {color:#81d50d;} \
    #fancytable1 {\
    background-color:;\
    width:500px;\
    border-top-left-radius:15px 15px;\
    -moz-border-radius-topleft:15px 15px;\
    border-radius-topleft:15px 15px;\
    border-top-right-radius:15px 15px;\
    -moz-border-radius-topright:15px 15px;\
    border-radius-topright:15px 15px;\
    font-family: Arial, Helvetica, sans-serif;\
    color: #222222;\
    border-collapse:collapse;\
    }\
    #fancytable1 th {\
    padding:5px;\
    border-top-left-radius:15px 15px;\
    -moz-border-radius-topleft:15px 15px;\
    border-radius-topleft:15px 15px;\
    border-top-right-radius:15px 15px;\
    -moz-border-radius-topright:15px 15px;\
    border-radius-topright:15px 15px;\
    font-size: 1.1em;\
    text-align: center;\
    background: -moz-linear-gradient(bottom, #666666, #222222);\
    background: -webkit-linear-gradient(top, #666666, #222222);\
    background: -o-linear-gradient(top, #666666,#222222);\
    background: linear-gradient(top, #666666, #222222);\
    color: #81d50d;\
    text-shadow: 2px 2px 2px #222222;\
    }\
    #fancytable1 tr.alt2 td {\
    padding:5px;\
    border-bottom-left-radius:15px 15px;\
    -moz-border-radius-bottomleft:15px 15px;\
    border-radius-bottomleft:15px 15px;\
    border-bottom-right-radius:15px 15px;\
    -moz-border-radius-bottomright:15px 15px;\
    border-radius-bottomright:15px 15px;\
    font-size: .75em;\
    text-align: center;\
    background: -moz-linear-gradient(bottom, #666666, #222222);\
    background: -webkit-linear-gradient(top, #666666, #222222);\
    background: -o-linear-gradient(top, #666666,#222222);\
    background: linear-gradient(top, #666666, #222222);\
    color: #81d50d;\
    text-shadow: 2px 2px 2px #222222;\
    }\
    #fancytable1 th {\
    font-size: 1em;\
    }\
    #fancytable1 td {\
    padding:5px;\
    background-color:#f0f0f0;\
    font-size: 1em;\
    }\
    #fancytable1 tr.alt td {\
    background-color: cccccc;\
    }\
    #fancytable1 tr:hover td {\
    background: -moz-linear-gradient(bottom, #666666, #222222) !important;\
    background: -webkit-linear-gradient(top, #666666, #222222);\
    background: -o-linear-gradient(top, #666666,#222222);\
    background: linear-gradient(top, #666666, #222222);\
    color:#81d50d;\
    }\
    </style>";
}
