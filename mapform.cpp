#include "mapform.h"
#include "ui_mapform.h"


#include <QtSql>

MapForm::MapForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MapForm)
{
    ui->setupUi(this);

    connect(ui->webView, SIGNAL(loadFinished(bool)), this, SLOT(showAll()));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(somethingChanged()));
    connect(ui->calendarWidget, SIGNAL(selectionChanged()), this, SLOT(somethingChanged()));
    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemClicked()));


    QSqlQuery query;
    query.exec("SELECT id, name FROM name");

    QString strAll;

    while (query.next())
    {
        ui->comboBox->addItem(query.value(1).toString(), query.value(0));
        strAll += query.value(0).toString() + ',';
    }

    strAll.chop(1);
    ui->comboBox->addItem("Alle", strAll);
}

MapForm::~MapForm()
{
    delete ui;
}

void MapForm::somethingChanged()
{
    ui->webView->load(QUrl("qrc:/html/google_maps.html"));
}

void MapForm::itemClicked()
{
    QPointF pt = ui->listWidget->currentItem()->data(Qt::UserRole).toPointF();
    showCoordinates(pt.x(), pt.y());
}


void MapForm::showAll()
{
    ui->listWidget->clear();

    QDate date = ui->calendarWidget->selectedDate();
    QString nameId = ui->comboBox->currentData().toString();

    QString timeStart = date.toString("yyyyMMdd") + "000000";
    QString timeEnd = date.toString("yyyyMMdd") + "235959";

    QString queryStr = QString("SELECT lat, lon, name FROM track WHERE name IN(%1) AND time BETWEEN %2 AND %3 ORDER BY name, id").arg(nameId).arg(timeStart).arg(timeEnd);

    qDebug() << queryStr;

    QSqlQuery query;
    query.exec(queryStr);

    double lat(0.0), lon(0.0);
    int oldName = 0;
    int newName = 0;

    QVector<QPair<double,double> > coords;


    while (query.next())
    {
        newName = query.value(2).toInt();
        if ((newName != oldName) && oldName != 0)
        {
            setMarker(lat, lon, oldName);
            oldName = newName;
            drawLine(coords);
            coords.clear();
        }

        lat = query.value(0).toDouble();
        lon = query.value(1).toDouble();
        coords.push_back(qMakePair(lon,lat));

        oldName = newName;
    }

    setMarker(lat, lon, newName);
    drawLine(coords);

    showCoordinates(lat, lon);
}


void MapForm::showCoordinates(double lat, double lon)
{
    QString str =
            QString("var newLoc = new google.maps.LatLng(%1, %2); ").arg(lat).arg(lon) +
            QString("map.setCenter(newLoc);") +
            QString("map.setZoom(18);");

    qDebug() << str;

    ui->webView->page()->runJavaScript(str);
}

void MapForm::drawLine(QVector<QPair<double,double> > const &coords)
{
    QString coordStr = "var taskCoordinates = [ ";

    for (auto coord : coords)
    {
        double lon = coord.first;
        double lat = coord.second;

        coordStr += "{lat:" + QString::number(lat) +  ", lng:" + QString::number(lon) + "},";
    }

    coordStr += "];\n";

    QString applyStr = "var thePath = new google.maps.Polyline({"
      "path: taskCoordinates,"
      "geodesic: true,"
      "strokeColor: '#FF0000',"
      "strokeOpacity: 1.0,"
      "strokeWeight: 2});"
      "thePath.setMap(map);\n";

    QString str = coordStr + applyStr;

    qDebug() << str;

    ui->webView->page()->runJavaScript(str);
}


void MapForm::setMarker(double lat, double lon, int nameId)
{
    QString queryStr = QString("SELECT name FROM name WHERE id =%1").arg(nameId);

    QSqlQuery query;
    query.exec(queryStr);

    QString caption("Empty");
    if (query.next())
    {
        caption = query.value(0).toString();
    }

    QString str =
            QString("var marker = new google.maps.Marker({") +
            QString("position: new google.maps.LatLng(%1, %2),").arg(lat).arg(lon) +
            QString("map: map,") +
            QString("title: %1").arg("\""+caption+"\"") +
            QString("});") +
            QString("markers.push(marker);");

    qDebug() << str;

    ui->webView->page()->runJavaScript(str);

    QPointF pt(lat,lon);

    QListWidgetItem *item = new QListWidgetItem(caption);
    item->setData(Qt::UserRole, pt);

    ui->listWidget->addItem(item);
}
